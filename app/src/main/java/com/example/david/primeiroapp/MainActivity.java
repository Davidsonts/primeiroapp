package com.example.david.primeiroapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private Button btn_limpar;
    private Button btn_confirmar;
    private EditText editar_texto;
    private TextView visualizar_texto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn_limpar = (Button)findViewById(R.id.btnLimpar);
        btn_confirmar = (Button)findViewById(R.id.btnConfirmar);
        editar_texto = (EditText)findViewById(R.id.editText);
        visualizar_texto = (TextView)findViewById(R.id.textView);

        // PROCEDURE OU COMO ABAIXO
//        btn_limpar.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                editar_texto.setText("");
//                // busca string
//                visualizar_texto.setText(getText(R.string.text_padrao));
//            }
//        });
    }

    // PROCEDURES

    public void limpar(View view){
        editar_texto.setText("");
        visualizar_texto.setText(getText(R.string.text_padrao));
    }

    public void confirmar(View view){
        visualizar_texto.setText(editar_texto.getText());
    }

}
